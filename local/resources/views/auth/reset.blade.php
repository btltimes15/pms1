@if (Session::has('error'))
  {{ trans(Session::get('reason')) }}
@endif

<div class="register col-lg-4 col-sm-4 col-xs-4">

    
    <form action="{{ url('/password/reset') }}" method="post">
        <input type="email" name="email" class="email form-control" placeholder="Email">
        <input type="password" name="password" placeholder="Password">

        <input type="password" name="password_confirmation" placeholder="Confirm Password">

        <input type="hidden" name="token" value="{{ csrf_token() }}">

        <input type="submit" name="submit" value="Reset Password">

</form>

</div>