<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CRM | Registration Page</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/square/blue.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition register-page">
        <div class="register-box">
            <div class="register-logo">
                <a href="../../index2.html"><b>CR</b>M</a>
            </div>

            <div class="register-box-body">
                <p class="login-box-msg">Register a new Account</p>

                <form method="POST" action="register">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{ old('name') }}">
                               <!--        <span class="glyphicon glyphicon-user form-control-feedback"></span>-->
                    </div>

                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Address" name="address">
              <!--        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
                    </div>


                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Phone" name="phone">
              <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control select2e" style="width: 100%;" tabindex="-1" aria-hidden="false" name="role">
                    <option selected="selected">Choose Role</option>
                    <option>Admin</option>
                    <option>Manager</option>
                    <option>User</option>
                </select>
                        <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password">
              <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
              <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
                    </div>



                    <div class="form-group has-feedback">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
                    </div>
                    <div class="row">

                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->

        <!-- jQuery 2.2.0 -->
        <script src="{{ URL::asset('plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- iCheck -->
        <script src="{{ URL::asset('plugins/iCheck/icheck.min.js') }}"></script>
        <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
        </script>
    </body>
</html>
