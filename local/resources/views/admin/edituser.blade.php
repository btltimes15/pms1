@include('admin/header')
<div class="register-box">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <h1><?php echo Session::get('message'); ?></h1>
    <div class="register-box-body">
        <p class="login-box-msg">Add New User</p>
       
        @foreach ($records as $record)
        <form method="POST" action="{{ url('userupdate/'.$record->id) }}">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{ $record->name }}">
                       <!--        <span class="glyphicon glyphicon-user form-control-feedback"></span>-->
            </div>

            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ $record->email }}">
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Address" name="address" value="{{ $record->address }}">
      <!--        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
            </div>


            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Phone" name="phone" value="{{ $record->phone }}">
      <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="form-group has-feedback">
                <select class="form-control select2e" style="width: 100%;" tabindex="-1" aria-hidden="false" name="role">
                    <option value="{{ $record->role }}">{{ $record->role }}</option>
                    <option value="admin">Admin</option>
                    <option value="manager">Manager</option>
                    <option value="user">User</option>
                </select>
                <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="form-group has-feedback">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="row">

                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
@endforeach

    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->
@include('admin/footer')
