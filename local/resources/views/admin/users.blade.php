@include('admin/header')

<div class="content-wrapper">


    <!-- Main content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users Details</h3>
                        @if (session('deleteuser'))

                        <div class="alert alert-success alert-dismissible" id="success_message">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                            {{ session('deleteuser') }}
                        </div> 
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Role</th>
                                    <th class="action">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($records as $record)    
                                <tr>
                                    <td>{{ $record->name }}</td>
                                    <td> {{ $record->email }}</td>
                                    <td>{{ $record->address }}</td>
                                    <td>{{ $record->phone }}</td>
                                    <td> {{ $record->role }}</td>
                                    <td><a href="user-edit/{{ $record->id }}"><img src="{{ asset('image/edit.png') }}"></a> | <a href="user-delete/{{ $record->id }}"><img src="{{ asset('image/Delete.png') }}" class="delete"></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Rule</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                        {!! $records->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- /.content -->
</div>

@include('admin/footer')
<script>
    $(".delete").on("click", function () {
        return confirm("Do you want to delete this item?");
    });
    $(function () {
        $('#example1').DataTable({
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]],
            "paging": true,
            "ordering": false,
            "info": false
        });

    });
</script>