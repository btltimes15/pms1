@include('admin/header')
<style>
    .content{
        min-height: 0px;
    }
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
        opacity: 1 !important;
        z-index: 9999 !important;
    }
</style>
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->

        <section class="content">

            <!-- START CUSTOM TABS -->
            <div class="row">


                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Reports</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Main content -->
                        <div class="tab-pane active" id="tab_1">
                            <section class="content">

                                <!-- START CUSTOM TABS -->
                                <form action="report-summary" method="post">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-2">
                                                </br>
                                                <b class="porospect">Prospect Report Summary</b>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label">Date From</label>

                                                    <div class="col-md-12">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="datepicker1" name="datestart"  data-date-format="dd/mm/yyyy" value="{{$datestart}}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label">Date To</label>

                                                    <div class="col-md-12">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="datepicker" name="dateend"  data-date-format="dd/mm/yyyy" value="{{$dateend}}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label">Optus SA Consultant</label>
                                                    <div class="col-md-12">
                                                        <select class="form-control select3" style="width: 100%;" name="optus">
                                                            <option value="All">All</option>
                                                            @foreach ($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label"></br></label>
                                                    <div class="col-md-12">
                                                        <select class="form-control select3" style="width: 100%;" name="type">
                                                            <option value="csv">CSV</option>
                                                            <option value="pdf">PDF</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label"></br></label>

                                                    <div class="col-md-12">
                                                        <button type="submit" name="submit" class="btn btn-info pull-right">Generate Report</button>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </form>
                            </section>  

                            <section class="content">

                                <form action="report-detail" method="post">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-2">
                                                </br>
                                                <b class="porospect">Prospect Report Detail</b>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label">Date From</label>

                                                    <div class="col-md-12">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="datepicker3" name="datestart"  data-date-format="dd/mm/yyyy" value="{{$datestart}}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label">Date To</label>

                                                    <div class="col-md-12">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="datepicker4" name="dateend"  data-date-format="dd/mm/yyyy" value="{{$dateend}}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label">Optus SA Consultant</label>

                                                    <div class="col-md-12">
                                                        <select class="form-control select3" style="width: 100%;" name="optus">
                                                            <option value="All">All</option>
                                                            @foreach ($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="inputPassword3" class="col-md-12 control-label"></br></label>
                                                    <div class="col-md-12">
                                                        <select class="form-control select3" style="width: 100%;" name="type">
                                                            <option value="csv">CSV</option>
                                                            <option value="pdf">PDF</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label"></br></label>

                                                    <div class="col-md-12">
                                                        <button type="submit" name="submit" class="btn btn-info pull-right">Generate Report</button>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </form>
                            </section>
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- END CUSTOM TABS -->

        </section>
        
        
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>
@include('admin/footer')
<script>

    $(function () {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });
        $('#datepicker3').datepicker({
            autoclose: true
        });
        $('#datepicker4').datepicker({
            autoclose: true
        });


    });

</script>