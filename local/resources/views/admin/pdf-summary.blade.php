<style>
    body{
        font-family: sans-serif;
    }
    .lara-su td {
        border: 1px solid;
        text-align: center;
        font-size: 12px;
    }
    .lara-su1{
        margin-top: 20px;
    }
    .lara-su1 td {
        border: 1px solid;
        text-align: center;
        font-size: 12px;
    }

    .lara-su {
        border: 1px solid;
        width: 100%;
        border-collapse: collapse;
    }
    .lara-su1 {
        border: 1px solid #3C8DBC;
        width: 100%;
        border-collapse: collapse;
    }
    tr.heading td{
        font-weight: bold;
        font-size: 14px;
        background: #ddd;
    }
    tr.noborder td{
        border: 0px;
    }
</style>
<table class="lara-su" >
    <tbody>
        <tr>
            <td style="font-weight: bold;"> Optus Consultant:</td>
            <td colspan="2">{{($optus=='All')?$optus:'Individual'}}</td>
        </tr>
        <tr>
            <td style="font-weight: bold;"> DATE:</td>
            <td colspan="2">1st{{ $date_start }} to {{ $dateend }}</td>
        </tr>
        <tr class="noborder">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <?php
        $total_mobile = '';
        $overall_total = '';
        ?>
        @foreach($records as $key=>$record)
        <?php
        $date = str_replace('-', '/', $key);
        $monthlydate = date('M, Y', strtotime($date));
        ?>
        <tr>
            <td colspan="3" style="font-weight: bold; font-size: 18px;">{{$monthlydate}}</td>
        </tr>
        <?php
        $total_mobile = '';
        if (count($record) > 0) {
            ?>
            <tr class="heading">
                <td>Name</td>
                <td style="text-align: left;">Mobile Contracts Overdue</td>
                <td>No of Mobiles</td>
            </tr>

            @foreach($record as $record_month)
            <?php $total_mobile = $total_mobile + $record_month->nomobiles; ?>
            <tr>
                <td>{{$record_month->username . ' Total against company (' . $record_month->name . ')'}}</td>
                <td>&nbsp;</td>
                <td>{{$record_month->nomobiles}}</td>
            </tr>

            @endforeach
            <?php $overall_total = $overall_total+$total_mobile; ?>
            <tr class="noborder">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>{{$monthlydate . ' Company Total for Mobiles'}}</td>
                <td>&nbsp;</td>
                <td>{{$total_mobile}}</td>
            </tr>
            <tr class="noborder">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <?php
        } else {
            ?>
            <tr class="noborder">
                <td colspan="3">Sorry No Record Found</td>
            </tr>
        <?php } ?>
        @endforeach

    </tbody>
</table>



<table class="lara-su1" >
    <tr class="noborder">
        <td style="background: #ddd; font-weight: bold; margin: 10px 0px;" colspan="2">Company Total for Mobiles</td>
        <td style="background: #ddd; margin: 10px 0px;">{{$overall_total}}</td>
    </tr>
</table>