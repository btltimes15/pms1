<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<div class="content-wrapper">


    <!-- Main content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Prospect Report ( Detail ) </h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="col-lg-12">

                            <div class="col-lg-12">

                                <div class="col-lg-4">
                                    Optus Consultant:
                                </div>
                                <div class="col-lg-8">
                                    @if($optus=='all')
                                    All
                                    @else
                                    Individual
                                    @endif
                                </div>

                            </div>
                            <div class="col-lg-12">

                                <div class="col-lg-4">
                                    Date:
                                </div>
                                <div class="col-lg-8">
                                    1st{{ $date_start }} to {{ $dateend }}
                                </div>

                            </div>
                            <?php $username = null; ?>
                            @foreach($records as $key=>$record)

                            <div class="col-sm-12" id="monthdate">
                                {{ date('M, Y', strtotime($key)) }}
                            </div> 
                            @foreach($record as $record_month)
                            <div class="col-lg-12">

                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                    <th>
                                        Name
                                    </th>

                                    <th>Mobile Contracts Overdue</th>
                                    <th>No of Mobiles</th>
                                    </thead>
                                    <tr>
                                        <td>
                                            @if($record_month->username != $username)
                                            {{ $record_month->username }}
                                            @endif 
                                        </td>
                                        <td></td>
                                        <td>{{ $record_month->nomobiles}}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Total for {{$record_month->username}}
                                        </td>
                                        <td></td>
                                        <td>{{ $record_month->nomobiles}}</td>
                                        <td></td>
                                    </tr>
                                </table>
                                <?php $username = $record_month->username; ?>
                            </div>
                            @endforeach
                            @endforeach
                        </div>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- /.content -->
</div>
