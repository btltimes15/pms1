@include('admin/header')
<style>
    .has-error{
        display: none;
    }
    
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company Form Elements
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <form class="form-horizontal" action="addCompany" method="post">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Company</h3>
                        @if (session('success'))

                        <div class="alert alert-success alert-dismissible" id="success_message">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                             {{ session('success') }}
                        </div> 
                        @endif
                    </div>
                   
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Company Name</label>
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Company Name" name="cname">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Company Address</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Company Address" name="caddress">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Suburb</label>

                                <div class="col-sm-8">
                                    <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                    <select class="form-control select2" style="width: 100%;" name="suburb">
                                        <option value="null">Choose Suburb</option>
                                        <option value="5159">5159</option>
                                        <option value="5000">5000</option>
                                        <option value="5558">5558</option>
                                        <option value="5311">5311</option>
                                        <option value="5014">5014</option>
                                        <option value="5154">5154</option>
                                        <option value="5173">5173</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">State</label>

                                <div class="col-sm-8">
                                    <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="State" name="state">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Post Code</label>

                                <div class="col-sm-8">
                                    <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Post Code" name="pcode">
                                </div>
                            </div>

                            <?php if (Auth::user()->role == 'admin') { ?>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label">Optus SA Consultant</label>

                                    <div class="col-sm-8">
                                        <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Optus SA Consultant</label></div>
                                        <select class="form-control select3" style="width: 100%;" name="optus">
                                            <option>Choose Consultant</option>
                                            @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            <?php } else {
                                ?>
                                   <input type="hidden" class="form-control" id="inputPassword3" name="optus" value="{{ Auth::user()->id }}">
                             <?php }
                            ?>
                        </div>

                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Services</h3>
                    </div>

                    <div class="box-body formcontact">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Level</label>

                            <div class="col-sm-8">

                                <select class="form-control select4" style="width: 100%;" name="level">
                                    <option value="Level 1">Level 1</option>
                                    <option value="Level 2">Level 2</option>
                                    <option value="Level 3">Level 3</option>
                                    <option value="Level 4">Level 4</option>
                                    <option value="Level 5">Level 5</option>
                                    <option value="Level 6">Level 6</option>
                                    <option value="Level 7">Level 7</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">N0 of Mobiles</label>

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" data-inputmask="'mask': ['999999999999999]', '+099999999999999']" data-mask  name="nummobiles">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Mobile End Contract</label>

                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="contractend">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Copy of Mobile Bill</label>

                            <div class="col-sm-8">
                                <input id="exampleInputFile" type="file" name="bill"> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">No of fixed lines</label>

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" data-inputmask="'mask': ['999999999999999]', '+099999999999999']" data-mask  name="numoflines">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Fixed Lines Contract End Date</label>

                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker1" name="fixedcontractend">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Copy of Fixed Bill</label>

                            <div class="col-sm-8">
                                <input id="exampleInputFile" type="file" name="fixedbill"> 
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Contacts</h3>
                        <a href="addcontact" class="addcontact btn btn-info pull-right">Add Contact</a>
                    </div>
                     <div class="box-body contactform">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            <tfoot>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Notes</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body">

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Notes</label>

                            <div class="col-sm-8">
                                <textarea class="form-control" rows="3" placeholder="Enter ..." name="notes"></textarea>

                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" name="submit" class="btn btn-info pull-right">Add Company</button>
                    </div>
                    <!-- /.box-footer -->
                   
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
   
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@include('admin/footer')
<script>
    $(function () {
        $("#example1").DataTable();

    });
</script>
<script>

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
        );

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>