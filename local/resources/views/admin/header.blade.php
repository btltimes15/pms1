<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CRM</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ URL::asset('dist/livecss/ioniicons.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('plugins/datatables/dataTables.bootstrap.css') }}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/flat/blue.css') }}">
        <!-- Morris chart -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/morris/morris.css') }}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/datepicker/datepicker3.css') }}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/all.css') }}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/timepicker/bootstrap-timepicker.min.css') }}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ URL::asset('plugins/select2/select2.min.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="skin-blue layout-boxed sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="{{ url('dashboard') }}" class="navbar-brand"><b>CR</b>M</a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                <?php
                                if (Auth::user()->role != 'user') {
                                    ?>
                                    <li class="dropdown">
                                        <a href="company" class="dropdown-toggle" data-toggle="dropdown">Company <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li class="active"><a href="{{ url('dashboard') }}">Company <span class="sr-only">(current)</span></a></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown">
                                        <a href="company" class="dropdown-toggle" data-toggle="dropdown">Users <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li class="active"><a href="{{ url('users') }}">Users <span class="sr-only">(current)</span></a></li>
                                            <li><a href="{{ url('adduser') }}">Add User</a></li>
                                        </ul>
                                    </li>
                                     <li><a href="{{ url('reports') }}">Reports</a></li>

                                   
                                <?php
                                } else{  ?>
                                    
                                    
                                           
                                    <li class="active"><a href="{{ url('addCompany') }}">Add Company</a></li>
                                    
                                <?php } ?>
                                    <li><a href="{{ url('logout') }}">Sign out</a></li>
                            </ul>

                        </div>
                        <!-- /.navbar-collapse -->
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Messages: style can be found in dropdown.less-->
                                <!--            <li class="dropdown messages-menu">
                                               Menu toggle button 
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-envelope-o"></i>
                                                <span class="label label-success">4</span>
                                              </a>
                                              <ul class="dropdown-menu">
                                                <li class="header">You have 4 messages</li>
                                                <li>
                                                   inner menu: contains the messages 
                                                  <ul class="menu">
                                                    <li> start message 
                                                      <a href="#">
                                                        <div class="pull-left">
                                                           User Image 
                                                          <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                                                        </div>
                                                         Message title and timestamp 
                                                        <h4>
                                                          Support Team
                                                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                        </h4>
                                                         The message 
                                                        <p>Why not buy a new awesome theme?</p>
                                                      </a>
                                                    </li>
                                                     end message 
                                                  </ul>
                                                   /.menu 
                                                </li>
                                                <li class="footer"><a href="#">See All Messages</a></li>
                                              </ul>
                                            </li>
                                             /.messages-menu 
                                
                                             Notifications Menu 
                                            <li class="dropdown notifications-menu">
                                               Menu toggle button 
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bell-o"></i>
                                                <span class="label label-warning">10</span>
                                              </a>
                                              <ul class="dropdown-menu">
                                                <li class="header">You have 10 notifications</li>
                                                <li>
                                                   Inner Menu: contains the notifications 
                                                  <ul class="menu">
                                                    <li> start notification 
                                                      <a href="#">
                                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                      </a>
                                                    </li>
                                                     end notification 
                                                  </ul>
                                                </li>
                                                <li class="footer"><a href="#">View all</a></li>
                                              </ul>
                                            </li>
                                             Tasks Menu 
                                            <li class="dropdown tasks-menu">
                                               Menu Toggle Button 
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-flag-o"></i>
                                                <span class="label label-danger">9</span>
                                              </a>
                                              <ul class="dropdown-menu">
                                                <li class="header">You have 9 tasks</li>
                                                <li>
                                                   Inner menu: contains the tasks 
                                                  <ul class="menu">
                                                    <li> Task item 
                                                      <a href="#">
                                                         Task title and progress text 
                                                        <h3>
                                                          Design some buttons
                                                          <small class="pull-right">20%</small>
                                                        </h3>
                                                         The progress bar 
                                                        <div class="progress xs">
                                                           Change the css width attribute to simulate progress 
                                                          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                          </div>
                                                        </div>
                                                      </a>
                                                    </li>
                                                     end task item 
                                                  </ul>
                                                </li>
                                                <li class="footer">
                                                  <a href="#">View all tasks</a>
                                                </li>
                                              </ul>
                                            </li>-->
                                <!-- User Account Menu -->
                                <li class="dropdown user user-menu">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <!-- The user image in the navbar-->

                                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                        @if(Auth::user()->role != 'user')
                                        <img src="{{ asset('image/admin.png') }}">
                                        @else
                                        <i class="fa fa-users"></i>
                                        @endif
                                        
                                        
                                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                                    </a>
                                    
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-custom-menu -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>
