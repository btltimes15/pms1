@include('admin/header')

<div class="content-wrapper">
    

    <!-- Main content -->
    
    <!-- Main content -->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
        

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Company Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Comp Name</th>
                  <th>Address</th>
                  <th>Suburb</th>
                  <th>Phone</th>
                  <th>Contact FName</th>
                  <th>Contact LName</th>
                  <th>Contact Mobile</th>
                  <th>Last Notes Entered</th>
                  <th>Created Date</th>
                  <th>Updated Date</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 4.0
                  </td>
                  <td>Win 95+</td>
                  <td> 4</td>
                  <td>X</td>
                  <td>Win 95+</td>
                  <td> 4</td>
                  <td>X</td>
                  <td>X</td>
                  <td>X</td>
                </tr>
               
                </tbody>
                <tfoot>
                <tr>
                  <th>Comp Name</th>
                  <th>Address</th>
                  <th>Suburb</th>
                  <th>Phone</th>
                  <th>Contact FName</th>
                  <th>Contact LName</th>
                  <th>Contact Mobile</th>
                  <th>Last Notes Entered</th>
                  <th>Created Date</th>
                  <th>Updated Date</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
    <!-- /.content -->
  </div>

@include('admin/footer')
<script>
  $(function () {
    $("#example1").DataTable();
    
  });
</script>