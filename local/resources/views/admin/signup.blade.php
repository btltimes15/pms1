@include('admin/header')
<div class="register-box">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (session('msg'))

    <div class="alert alert-success alert-dismissible" id="success_message">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
        <h4><i class="icon fa fa-check"></i> Congratulation</h4>
        {{ session('msg') }}
    </div> 
    @endif
    <div class="register-box-body">
        <p class="login-box-msg">Add New User</p>

        <form method="POST" action="adduser">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Full Name" name="name">
                       <!--        <span class="glyphicon glyphicon-user form-control-feedback"></span>-->
            </div>

            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email">
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Address" name="address">
      <!--        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
            </div>


            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Phone" name="phone">
      <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="form-group has-feedback">
                <select class="form-control select2e" style="width: 100%;" tabindex="-1" aria-hidden="false" name="role">
                    <option selected="selected">Choose Role</option>
                    <option value="admin">Admin</option>
                    <option value="manager">Manager</option>
                    <option value="user">User</option>
                </select>
                <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
      <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
      <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>



            <div class="form-group has-feedback">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <!--        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>-->
            </div>
            <div class="row">


                <div class="col-xs-4">
                    <a href="{{ url('users') }}" class="btn btn-primary margin pull-right">Cancel</a>
                </div> 
                <div class="col-xs-3">
                    <button type="submit" class="btn btn-primary margin pull-right">Register</button>
                </div>
                <!-- /.col -->
            </div>
        </form>


    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->
@include('admin/footer')
