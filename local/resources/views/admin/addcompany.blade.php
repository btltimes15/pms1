@include('admin/header')
<style>
    .has-error{
        display: none;
    }

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <div class="nav-tabs-custom">
       
        <div class="tab-content">
            <!-- Main content -->
            <div class="tab-pane active" id="tab_1">
                <form class="form-horizontal" action="addCompany" method="post" enctype="multipart/form-data">
                    <section class="content">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-6">

                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Company</h3>
                                        @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        @if (session('success'))

                                        <div class="alert alert-success alert-dismissible" id="success_message">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                            <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                                            {{ session('success') }}
                                        </div> 
                                        @endif
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Company Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputEmail3" placeholder="Company Name" name="cname">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">ABN / ACN</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputEmail3" placeholder="ABN / ACN" name="abn">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Company Address</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputPassword3" placeholder="Company Address" name="caddress">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Suburb</label>

                                            <div class="col-sm-8">
                                                <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                                <select class="form-control select2" style="width: 100%;" name="suburb" onchange="getsubrubvalue()" id="suburb">
                                                    <option>Choose Suburb</option>
                                                    @foreach($suburb as $sub)
                                                    <option value="{{$sub->id}}">{{$sub->suburb}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" id="getsubrub" name="getsubrub" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">State</label>

                                            <div class="col-sm-8">
                                                <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                                <input type="text" class="form-control" id="state" placeholder="State" name="state">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Post Code</label>

                                            <div class="col-sm-8">
                                                <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                                <input type="text" class="form-control" id="pcode" placeholder="Post Code" name="pcode">
                                            </div>
                                        </div>

                                        <?php if (Auth::user()->role != 'user') { ?>
                                            <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-4 control-label">Optus SA Consultant</label>

                                                <div class="col-sm-8">
                                                    <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Optus SA Consultant</label></div>
                                                    <select class="form-control select3" style="width: 100%;" name="optus">
                                                        <option>Choose Consultant</option>
                                                        @foreach ($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else {
                                            ?>
                                            <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-4 control-label">Optus SA Consultant</label>

                                                <div class="col-sm-8">
                                                    <input type="hidden" class="form-control" id="inputPassword3" name="optus" value="{{ Auth::user()->id }}"><lable class='userslected'>{{ Auth::user()->name }}</lable>
                                                </div>
                                            </div>
                                        <?php }
                                        ?>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">
                                                <div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>Input with error</label></div>
                                                <select class="form-control select3" style="width: 100%;" name="status">
                                                    <option>Choose Status</option>
                                                    <option value="Prospect">Prospect</option>
                                                    <option value="Customer">Customer</option>
                                                    <option value="Wot">Wot</option>
                                                    <option value="Other Channel">Other Channel</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Services</h3>
                                    </div>

                                    <div class="box-body formcontact">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Level</label>

                                            <div class="col-sm-8">

                                                <select class="form-control select4" style="width: 100%;" name="level">
                                                    <option>Choose Level</option>
                                                    <option value="Micro (1 - 5)">Micro (1 - 5)</option>
                                                    <option value="Small (6 - 15)">Small (6 - 15)</option>
                                                    <option value="Mid (16 - 30)">Mid (16 - 30)</option>
                                                    <option value="Fleet (30+)">Fleet (30+)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">No of Mobiles</label>

                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <input type="text" class="form-control" data-inputmask="'mask': ['999']" data-mask  name="nummobiles">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Mobile Contract End Date</label>

                                            <div class="col-sm-8">
                                                <!--<input type="text" class="form-control pull-right" name="contractend">-->
                                                 <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker2" name="contractend"  data-date-format="dd/mm/yyyy">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Copy of Mobile Bill</label>

                                            <div class="col-sm-8">
                                                <input id="exampleInputFile" type="file" name="bill"> 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">No of fixed lines</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" data-inputmask="'mask': ['999']" data-mask  name="numoflines">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Fixed Lines Contract End Date</label>

                                            <div class="col-sm-8">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker1" name="fixedcontractend"  data-date-format="dd/mm/yyyy">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Copy of Fixed Bill</label>

                                            <div class="col-sm-8">
                                                <input id="exampleInputFile" type="file" name="fixedbill"> 
                                            </div>
                                        </div>

                                    </div>

                                </div>


                            </div>

                            <div class="col-md-6">

                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Contacts</h3>
                                        <a href="addcontact" class="addcontact btn btn-info pull-right">Add Contact</a>
                                    </div>
                                    <div class="modal modal-primary">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Add Contact</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>You can add contact once company will be added first.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                            </div>
                                    <div class="box-body contactform">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>Action</th>
                                                </tr>
                                            <tfoot>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Notes</h3>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-7 control-label">Calendar</label>

                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker" name="followupdate"  data-date-format="dd/mm/yyyy" data-date-start-date="0d"  data-date-end-date="+60d" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <!-- form start -->

                                    <div class="box-body">

                                        <div class="form-group">


                                            <div class="col-sm-12">
                                                <textarea class="form-control" rows="10" placeholder="Enter ..." name="notes"></textarea>

                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" name="submit" class="btn btn-info margin pull-right">Add Company</button>
                                        <a href="{{ url('dashboard') }}" class="btn btn-primary margin pull-right">Cancel</a>
                                    </div>
                                    <!-- /.box-footer -->

                                </div>

                            </div>
                            <!--/.col (right) -->
                        </div>

                        <!-- /.row -->
                    </section>
                </form>
            </div>

           
        </div>
    </div>
    <!-- /.content -->
</div>

@include('admin/footer')
<script>
    $(function () {
        $("#example1").DataTable();
        $('.addcontact').click(function () {
                $('.modal.modal-primary').css('display', 'block');
                return false;
        });
        $('.close').click(function () {
            var count = $('#countfile').val();
            $('.modal.modal-primary').css('display', 'none');
        });
        $('.btn-outline').click(function () {
            var count = $('#countfile').val();
            $('.modal.modal-primary').css('display', 'none');
        });

    });
    
    
    
    function getsubrubvalue() {
        var subr = $('#suburb').val();
        $.ajax({
            url: "getsubrub",
            type: "post", //send it through get method
            data: {id: +subr},
            dataType: 'JSON',
            success: function (response) {
                $('#state').val(response.state);
                $('#pcode').val(response.postcode);
                $('#getsubrub').val(response.suburb);
                //alert(response.state);
            },
            error: function (xhr) {
                //Do Something to handle error
            }
        });

    }
</script>
<script>

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
                }
        );

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });
        $('#datepicker2').datepicker({
            autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>