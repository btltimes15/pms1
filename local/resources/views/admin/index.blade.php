@include('admin/header')
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->

        <section class="content">

            <!-- START CUSTOM TABS -->
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <h2 class="page-header">CRM</h2>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ url('addCompany') }}" class="btn btn-info margin pull-right">Add Company</a>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        @if (session('deletecompany'))

                        <div class="alert alert-success alert-dismissible" id="success_message">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                            {{ session('deletecompany') }}
                        </div> 
                        @endif
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">My Prospects</a></li>
                                <li><a href="#tab_2" data-toggle="tab">All Prospects</a></li>
                                <li><a href="#tab_3" data-toggle="tab">My Follow Ups</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                                <th class="action">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($userrecord as $record) {
                                                ?>

                                                <tr>
                                                    <td><a href="company-edit/<?php echo $record->id; ?>"><?php echo $record->name; ?></a></td>
                                                    <td><?php echo $record->caddress; ?></td>
                                                    <td><?php echo $record->suburb; //$subrub = $notes->getsuburbdata($record->suburb); echo $subrub[0]->suburb;           ?></td>
                                                    <td><?php echo $record->phone; ?></td>
                                                    <td><?php echo $record->fname; ?></td>
                                                    <td><?php echo $record->lname; ?></td>
                                                    <td> <?php echo $record->mobile; ?></td>
                                                    <td class="lastnote">
                                                        <?php
//                                                        $noteee =  DB::select( DB::raw('select notes from notes where companyid='.$record->id.' order by created_at desc limit 0,1'));
//                                                        $resultArray = json_decode(json_encode($noteee), true);
//                                                        foreach ($resultArray as $resultArrays){
//                                                            echo $resultArrays['notes'];
//                                                        }
                                                        $noteee = $notes->getnote1($record->id);
                                                        echo $noteee->notes;
                                                        ?>
                                                    </td>
                                                    <td><a href="company-edit/<?php echo $record->id; ?>"><img src="{{ asset('image/edit.png') }}"></a></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    {!! $userrecord->render() !!}
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                                @if(Auth::user()->role=='admin')
                                                <th>Action</th>
                                                @endif

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($records as $record) { ?>
                                                <tr>
                                                    <td><a href="company-edit/<?php echo $record->id; ?>"><?php echo $record->name; ?></a></td>
                                                    <td><?php echo $record->caddress; ?></td>
                                                    <td><?php echo $record->suburb; ?></td>
                                                    <td><?php echo $record->phone; ?></td>
                                                    <td><?php echo $record->fname; ?></td>
                                                    <td><?php echo $record->lname; ?></td>
                                                    <td> <?php echo $record->mobile; ?></td>
                                                    <td class="lastnote">
                                                        <?php
                                                        //$noteee =  DB::select( DB::raw('select notes from notes where companyid='.$record->id.' order by created_at desc limit 0,1'));
                                                        $noteee = $notes->getnote1($record->id);
                                                        echo $noteee->notes;
//                                                        $resultArray = json_decode(json_encode($noteee), true);
//                                                        foreach ($resultArray as $resultArrays){
//                                                            echo $resultArrays['notes'];
//                                                        }
                                                        ?>
                                                    </td>

                                                    <td><a href="company-edit/<?php echo $record->id; ?>"><img src="{{ asset('image/edit.png') }}"></a></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                                @if(Auth::user()->role=='admin')
                                                <th>Action</th>
                                                @endif
                                            </tr>
                                        </tfoot>
                                    </table>
                                    {!! $records->render() !!}

                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <table id="example3" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Follow Up Date</th>
                                                <th>Last Notes Entered</th>
                                                <th class="action">Mark As Complete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $count_follow = 1;
                                            if (isset($followup)) {
                                                foreach ($followup as $record) {
                                                    ?>
                                                    <tr class="rowid_<?php echo $count_follow; ?>">
                                                <input type="hidden" name="noteid" value="<?php echo $record->noteid; ?>" id="noteid_<?php echo $count_follow; ?>">
                                                <input type="hidden" name="companyid" value="<?php echo $record->id; ?>" id="companyid_<?php echo $count_follow; ?>">
                                                <input type="hidden" name="rowid" value="<?php echo $count_follow; ?>" id="rowid_<?php echo $count_follow; ?>">
                                                <td><a href="company-edit/<?php echo $record->id; ?>"><?php echo $record->name; ?></a></td>
                                                <td><?php echo $record->caddress; ?></td>
                                                <td><?php echo $record->suburb; ?></td>
                                                <td><?php echo $record->phone; ?></td>
                                                <td> 
                                                    <?php
                                                    echo date('d, F y', strtotime($record->followupdate));
                                                    ?></td>
                                                <td class="lastnote"><?php echo $record->notes; ?></td>
                                                <td class="action"><a href="#" class="delete" onclick="deletenote(<?php echo $count_follow; ?>)"><img src="{{ asset('image/checkmark.png') }}"></a></td>
                                                </tr>
                                                <?php
                                                $count_follow++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Follow Up Date</th>
                                                <th>Last Notes Entered</th>
                                                <th class="action">Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->
                <!-- END CUSTOM TABS -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>
@include('admin/footer')
<script>
    function deletenote(id) {
        //alert(id);
        // if (confirm("Do you want to delete this item?") == true) {

        var companyid = $('#companyid_' + id).val();
        //var rowid = $('#rowid').val();
        var noteid = $('#noteid_' + id).val();
        //alert(noteid); return false;
        $.ajax({
            type: "POST",
            url: 'deletenote',
            data: {companyid: companyid, noteid: noteid},
            success: function (msg) {
                $('.rowid_' + id).remove();
            }
        })
        //}
    }
    $(document).ready(function () {

    });
    $(function () {
        $('#example2, #example1, #example3').DataTable({
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]],
            "paging": true,
            "ordering": false,
            "info": false
        });
    });

</script>