@include('admin/header')
<style>
    .has-error{
        display: none;
    }

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company Form Elements
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    @foreach ($records as $record)
    <form class="form-horizontal" action="{{ url('company-update/'.$record->companyid) }}" method="post">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Company</h3>
                            @if (session('success'))

                            <div class="alert alert-success alert-dismissible" id="success_message">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                                {{ session('success') }}
                            </div> 
                            @endif
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Company Name</label>

                                <div class="col-sm-8">
                                    {{$record->name}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Company Address</label>

                                <div class="col-sm-8">
                                    {{$record->caddress}}
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">State</label>

                                <div class="col-sm-8">
                                    {{$record->state}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Post Code</label>

                                <div class="col-sm-8">
                                    {{$record->postcode}}
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Optus SA Consultant</label>

                                <div class="col-sm-8">
                                    {{ Auth::user()->name }}
                                </div>

                            </div>
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Services</h3>
                                </div>

                                <div class="box-body formcontact">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Level</label>

                                        <div class="col-sm-8">
                                            {{$record->level}}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">N0 of Mobiles</label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                {{$record->nomobiles}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Mobile End Contract</label>

                                        <div class="col-sm-8">
                                            <div class="input-group date">
                                                {{ $record->mobcontenddate}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Copy of Mobile Bill</label>

                                        <div class="col-sm-8">
                                            {{ $record->mobcontenddate}} 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">No of fixed lines</label>

                                        <div class="col-sm-8">
                                            <div class="input-group">

                                                {{ $record->lines}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Fixed Lines Contract End Date</label>

                                        <div class="col-sm-8">
                                            <div class="input-group date">

                                                {{ $record->fixlineenddate}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Copy of Fixed Bill</label>

                                        <div class="col-sm-8">
                                            <input id="exampleInputFile" type="file" name="fixedbill"> 
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Contacts</h3>
                            <!--                                    <a href="{{ url('addcontact/'.$record->id) }}" class="addcontact btn btn-info pull-right">Add Contact</a>
                                                            </div>-->
                            <div class="box-body contactform">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    @foreach($contacts as $contact)
                                    <tr>
                                        <td>{{$contact->fname}}</td>
                                        <td>{{$contact->lname}}</td>
                                        <td>{{$contact->mobile}}</td>
                                        <td>{{$contact->email}}</td>
                                    </tr>
                                    @endforeach
                                    <tfoot>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div> 
                        @endforeach
                        <!-- general form elements disabled -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Notes</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            </form>
                            <form method='post' action='{{ url('add-note/'.$record->companyid) }}'>
                                <div class="box-body">
                                    <div class="box-footer">
                                        <a href="{{ url('show-note/'.$record->companyid) }}" class="btn btn-info pull-right">Show Notes</a>
                                    </div>
                                    @if (session('addnote'))

                                    <div class="alert alert-success alert-dismissible" id="success_message">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                        <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                                         {{ session('addnote') }}
                                    </div> 
                                    @endif
                                    <div class="form-group">

                                        <label for="inputPassword3" class="col-sm-4 control-label">Notes</label>

                                        <div class="col-sm-8">
                                            <textarea class="form-control" rows="3" placeholder="Enter ..." name="notes" value="{{$record->notes}}" required="required">{{$record->notes}}</textarea>

                                        </div>
                                    </div>
                                    <input type="hidden" name="userid" value="{{Auth::user()->id}}">
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" name="submit" class="btn btn-info pull-right">Add Note</button>
                                </div>
                            </form>
                            <!-- /.box-footer -->

                        </div>
                        <!-- /.box -->
                    </div>
                    <!--/.col (right) -->
                </div>

                <!-- /.row -->
        </section>
        <!-- /.content -->
</div>

@include('admin/footer')
<script>

    function getsubrubvalue() {
        var subr = $('#suburb').val();
        // alert(subr);
        $.ajax({
            url: "/pms/getsubrub",
            type: "post", //send it through get method
            data: {id: +subr},
            dataType: 'JSON',
            success: function (response) {
                $('#state').val(response.state);
                $('#pcode').val(response.postcode);
            },
            error: function (xhr) {
                //Do Something to handle error
            }
        });

    }
    // End add company form Code

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
        );

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>