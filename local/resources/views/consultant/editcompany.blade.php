@include('admin/header')
<style>
    .has-error{
        display: none;
    }

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company Details
        </h1>
       
    </section>
    <?php //print_r($records[0]); exit; ?>
@foreach ($records as $record)
 
    <form class="form-horizontal" action="{{ url('company-update/'.$record->companyid) }}" method="post" enctype="multipart/form-data">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Company</h3>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if (session('success'))

                            <div class="alert alert-success alert-dismissible" id="success_message">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                                {{ session('success') }}
                            </div> 
                            @endif
                        </div>
                        
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Company Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Company Name" name="cname" value="{{$record->name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Company Address</label>

                                <div class="col-sm-8">
                                      <input type="text" class="form-control" id="inputPassword3" placeholder="Company Address" name="caddress" value="{{$record->caddress}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Suburb</label>
                                    <div class="col-sm-8">

                                          <select class="form-control select2" style="width: 100%;" name="suburb" onchange="getsubrubvalue()" id="suburb">

                                               @foreach($suburb as $sub)
                                               <option <?php if($record->suburb == $sub->suburb){ echo "selected='selected'"; } ?> value="{{$sub->id}}">{{$sub->suburb}}</option>
                                               @endforeach
                                           </select>
                                         <input type="hidden" id="getsubrub" name="getsubrub" value="{{$record->suburb}}">
                                   </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">State</label>

                                <div class="col-sm-8">
                                     <input type="text" class="form-control" id="state" placeholder="State" name="state" value="{{$record->state}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Post Code</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pcode" placeholder="Post Code" name="pcode" value="{{$record->postcode}}">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Optus SA Consultant</label>

                                <div class="col-sm-8">
                                  <input type="hidden" class="form-control" id="inputPassword3" name="optus" value="{{ Auth::user()->id }}">{{ Auth::user()->name }}
                                </div>

                            </div>
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Services</h3>
                                </div>

                                <div class="box-body formcontact">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Level</label>

                                        <div class="col-sm-8">
                                            <select class="form-control select4" style="width: 100%;" name="level">
                                        <option value="{{$record->level}}">{{$record->level}}</option>
                                        <option value="Micro (1 - 5)">Micro (1 - 5)</option>
                                        <option value="Small (6 - 15)">Small (6 - 15)</option>
                                        <option value="Mid (16 - 30)">Mid (16 - 30)</option>
                                        <option value="Fleet (30+)">Fleet (30+)</option>
                                    </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">N0 of Mobiles</label>
                                         <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="text" class="form-control" data-inputmask="'mask': ['999999999999999]', '+099999999999999']" data-mask  name="nummobiles" value="{{$record->nomobiles}}">
                                    </div>
                                </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Mobile End Contract</label>
                                <div class="col-sm-8">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker" name="contractend" value="<?php echo $record->mobcontenddate; ?>"  data-date-format="dd/mm/yyyy">
                                    </div>
                                </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Copy of Mobile Bill</label>

                                        <div class="col-sm-8">
                                           <input id="exampleInputFile" type="file" name="bill" value="{{ $record->mobilebill}}"><a href="{{url('local/file/bill/'.$record->mobilebill)}}" download> {{$record->mobilebill}}</a>
                                    <input type="hidden" name="secondmobile" value="{{ $record->mobilebill }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">No of fixed lines</label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                 <input type="text" class="form-control" data-inputmask="'mask': ['999999999999999]', '+099999999999999']" data-mask  name="numoflines" value="{{$record->lines}}">
                                              
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Fixed Lines Contract End Date</label>
                                <div class="col-sm-8">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker1" name="fixedcontractend" value="<?php echo $record->fixlineenddate; ?>"  data-date-format="dd/mm/yyyy">
                                    </div>
                                </div>
                                       
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Copy of Fixed Bill</label>

                                        <div class="col-sm-8">
                                             <input id="exampleInputFile" type="file" name="fixedbill" value="{{$record->fixedbill}}"> <a href="{{url('local/file/bill/'.$record->fixedbill)}}" download> {{$record->fixedbill}}</a>
                                    <input type="hidden" name="secondfixedbill" value="{{$record->fixedbill}}"> 
                                        </div>
                                    </div>
                                    
                                     <div class="box-footer">
                                    <button type="submit" name="submit" class="btn btn-info pull-right">Update Company</button>
                                </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div> </form>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Contacts</h3>
                           <a href="{{ url('addcontact/'.$record->id) }}" class="addcontact btn btn-info pull-right">Add Contact</a>
                            <div class="box-body contactform">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    @foreach($contacts as $contact)
                                    <tr>
                                        <td>{{$contact->fname}}</td>
                                        <td>{{$contact->lname}}</td>
                                        <td>{{$contact->mobile}}</td>
                                        <td>{{$contact->email}}</td>
                                    </tr>
                                    @endforeach
                                    <tfoot>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div> 
                       
                        <!-- general form elements disabled -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Notes</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                           
                            <form method='post' action='{{ url('add-note/'.$record->companyid) }}'>
                                <div class="box-body">
                                    <div class="box-footer">
                                        <a href="{{ url('show-note/'.$record->companyid) }}" class="btn btn-info pull-right">Show Notes</a>
                                    </div>
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    @if (session('addnote'))

                                    <div class="alert alert-success alert-dismissible" id="success_message">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                        <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                                        {{ session('addnote') }}
                                    </div> 
                                    @endif
                                    <div class="form-group">

                                       
@endforeach
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="10" placeholder="Enter ..." name="notes" value="{{$record->cnotes}}" required="required">{{$record->cnotes}}</textarea>

                                        </div>
                                    </div>
                                    <input type="hidden" name="userid" value="{{Auth::user()->id}}">
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" name="submit" class="btn btn-info pull-right">Add Note</button>
                                </div>
                            </form>
                            <!-- /.box-footer -->

                        </div>
                        <!-- /.box -->
                    </div>
                    <!--/.col (right) -->
                </div>

                <!-- /.row -->
        </section>
        <!-- /.content -->
</div>

@include('admin/footer')
<script>

    function getsubrubvalue() {
        var subr = $('#suburb').val();
        // alert(subr);
        $.ajax({
            url: "/pms/getsubrub",
            type: "post", //send it through get method
            data: {id: +subr},
            dataType: 'JSON',
            success: function (response) {
                 $('#state').val(response.state);
                $('#pcode').val(response.postcode);
                $('#getsubrub').val(response.suburb);
            },
            error: function (xhr) {
                //Do Something to handle error
            }
        });

    }
    // End add company form Code

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
        );

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>