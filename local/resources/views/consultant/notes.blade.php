@include('admin/header')
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->

        <section class="content">

            <!-- START CUSTOM TABS -->
            <h2 class="page-header">CRM</h2>

            <div class="row">
                <div class="col-md-12">
                    @if (session('deletecompany'))

                    <div class="alert alert-success alert-dismissible" id="success_message">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                        {{ session('deletecompany') }}
                    </div> 
                    @endif
                    <div class="nav-tabs-custom">

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>

                                            <th>#ID</th>
                                            <th>User Name</th>
                                            <th>Note</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $contant = 1 ; ?>
                                        @foreach ($records as $record)


                                        <tr>
                                            <td>{{@ $contant++ }}</td>
                                            <td>Entered by <strong>{{Auth::user()->name}}</strong> on {{ date('d, F y', strtotime($record->created_at)) }}</td>
                                            <td><?php echo $record->notes; ?></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                             <th>#ID</th>
                                            <th>User Name</th>
                                            <th>Note</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>


                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->
            <!-- END CUSTOM TABS -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>
@include('admin/footer')
<script>
    $(function () {
        $("#example1").DataTable();
        $("#example2").DataTable();

    });
</script>