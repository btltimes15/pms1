@include('admin/header')
<style>
    .has-error{
        display: none;
    }

</style>
<div class="content-wrapper">

    <!-- Main content -->
    <form class="form-horizontal" action="{{ url('addcontact')}}" method="post">
        <section class="content">
            <div class="row">
                <!-- left column -->

                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Contacts</h3>
                            @if (session('contactadd'))

                        <div class="alert alert-success alert-dismissible" id="success_message">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                             {{ session('contactadd') }}
                        </div> 
                        @endif
                        </div>


                        <div class="box-body formcontact">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">First Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="First Name" name="fname">
                                    <input type="hidden" class="form-control"  name="companyid" value="{{$id}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Last Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Last Name" name="lname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Mobile</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Mobile" name="mobile">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Email</label>

                                <div class="col-sm-8">
                                    <input type="email" class="form-control" id="inputPassword3" placeholder="Email" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Position</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Position" name="position">
                                </div>
                                
                            </div>
                            <div class="box-footer">
                           
                            <button type="submit" name="submit" class="btn btn-info pull-right margin">Add Contact</button>
                            <a href="{{ url('company-edit/'.$id) }}" class="btn btn-primary margin pull-right">Go Back</a>
                        </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!--/.col (right) -->
                </div>
                </form>
                <!-- /.row -->
        </section>
        <!-- /.content -->
</div>

@include('admin/footer')
<script>

    // Form Submit add company
//    $("form").on("submit", function (event) {
//        event.preventDefault();
//        // $("form").serialize()
//        //console.log($(this).serialize());   //serialize form on client side
//        //.$('#success_message').css('display','block');
//        $.ajax({
//            url: 'addcopanyform',
//            type: 'POST',
//            data: $(this).serialize(),
//            dataType: 'JSON',
//            success: function (data) {
//                $('#success_message').css('display', 'block');
//                //console.log(data);
//            }
//        });
//    });

// End add company form Code

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
        );

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>