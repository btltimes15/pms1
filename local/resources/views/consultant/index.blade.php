@include('admin/header')
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->

        <section class="content">

            <!-- START CUSTOM TABS -->
            <h2 class="page-header">CRM</h2>

            <div class="row">
                <div class="col-md-12">
                    @if (session('deletecompany'))

                        <div class="alert alert-success alert-dismissible" id="success_message">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <h4><i class="icon fa fa-check"></i> Congratulation</h4>
                             {{ session('deletecompany') }}
                        </div> 
                        @endif
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">My Prospects</a></li>
                            <li><a href="#tab_2" data-toggle="tab">All Prospects</a></li>
                            <!--              <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                              Dropdown <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                              <li role="presentation" class="divider"></li>
                                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                                            </ul>
                                          </li>-->

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php
                                            foreach ($records as $record) {
                                                if($record->optus == Auth::user()->id){
                                                ?>
                                           
                                                    <tr>
                                                        <td><a href="{{ url('company-edit/'.$record->id) }}"><?php echo $record->name; ?></a></td>
                                                        <td><?php echo $record->caddress; ?></td>
                                                        <td><?php   echo $record->suburb; //$subrub = $notes->getsuburbdata($record->suburb); echo $subrub[0]->suburb; ?></td>
                                                        <td><?php echo $record->phone; ?></td>
                                                        <td><?php echo $record->fname; ?></td>
                                                        <td><?php echo $record->lname; ?></td>
                                                        <td> <?php echo $record->mobile; ?></td>
                                                        <td><?php   echo $record->cnotes;//$noteee = $notes->getnote($record->id); echo $noteee->notes;   ?></td>
                                                        <td><a href="company-edit/<?php echo $record->id; ?>"><img src="{{ asset('image/edit.png') }}"></a></td>
                                                        </tr>
                                            <?php }
                                            
                                                } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($records as $record) { ?>
                                                <tr>
                                                     <td><?php echo $record->name; ?></td>
                                                    <td><?php echo $record->caddress; ?></td>
                                                    <td><?php echo $record->suburb; ?></td>
                                                    <td><?php echo $record->phone; ?></td>
                                                    <td><?php echo $record->fname; ?></td>
                                                    <td><?php echo $record->lname; ?></td>
                                                    <td> <?php echo $record->mobile; ?></td>
                                                   <td><?php   echo $record->cnotes;//$noteee = $notes->getnote($record->id); echo $noteee->notes;   ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Comp Name</th>
                                                <th>Address</th>
                                                <th>Suburb</th>
                                                <th>Phone</th>
                                                <th>Contact FName</th>
                                                <th>Contact LName</th>
                                                <th>Contact Mobile</th>
                                                <th>Last Notes Entered</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                            </div>
                            <!-- /.tab-pane -->

                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->
            <!-- END CUSTOM TABS -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</div>
@include('admin/footer')

<script>
  $(function () {
   $('#example2').DataTable( {
       "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]]
    } );
   $('#example1').DataTable( {
        "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]]
    } );
    
  });
</script>