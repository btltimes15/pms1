<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllprospectusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allprospectuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('caddress');
            $table->string('suburb', 100);
            $table->string('state', 100);
            $table->string('postcode', 100);
            $table->string('optus', 100);
            $table->string('cnotes', 255);
            $table->string('status', 100);
            $table->string('abn', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('allprospectuses');
    }
}
