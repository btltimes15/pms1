<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class Allprospectusmodel extends Model {

    public function newcompanycreate($request) {
        $current = Carbon::now();
        if ($request->file('bill')) {
            $name = $request->file('bill')->getClientOriginalName();
            $request->file('bill')->move('local/file/bill', $name);
        } else {
            $name = '';
        }
        if ($request->file('fixedbill')) {
            $name1 = $request->file('fixedbill')->getClientOriginalName();
            $request->file('fixedbill')->move('local/file/flexibill', $name1);
        } else {
            $name1 = '';
        }

        $data = array('name' => $request->input('cname'), 'caddress' => $request->input('caddress'), 'suburb' => $request->input('getsubrub'), 'state' => $request->input('state')
            , 'postcode' => $request->input('pcode'), 'optus' => $request->input('optus'), 'cnotes' => $request->input('notes'), 'status' => $request->input('status'), 'abn' => $request->input('abn'), 'created_at' => $current, 'updated_at' => $current);
        $last_insertid = DB::table('allprospectuses')->insertGetId($data);

        // Services Table insert data
        $services = array('level' => $request->input('level'), 'nomobiles' => str_replace('_', '', $request->input('nummobiles')), 'mobcontenddate' => $request->input('contractend'), 'mobilebill' => $name, 'lines' => str_replace('_', '', $request->input('numoflines'))
            , 'fixlineenddate' => $request->input('fixedcontractend'), 'fixedbill' => $name1, 'companyid' => $last_insertid, 'created_at' => $current, 'updated_at' => $current);
        DB::table('services')->insert($services);

        $followup = str_replace('/', '-', $request->input('followupdate'));
        $time = date(' H:i:s', strtotime($current));
        $followup = date('Y-m-d' . ' ' . $time, strtotime($followup));
        $notes = array('notes' => $request->input('notes'), 'userid' => $request->input('optus'), 'companyid' => $last_insertid, 'followupdate' => $followup, 'created_at' => $current, 'updated_at' => $current);
        DB::table('notes')->insert($notes);
        return $last_insertid;
    }

    public function updatecompany($request, $id) {
        $current = Carbon::now();

        if ($request->file('bill')) {
            $name = $request->file('bill')->getClientOriginalName();
            $request->file('bill')->move('local/file/bill', $name);
        } else {
            $name = $request->input('secondmobile');
        }

        if ($request->file('fixedbill')) {
            $name1 = $request->file('fixedbill')->getClientOriginalName();
            $request->file('fixedbill')->move('local/file/flexibill', $name1);
        } else {
            $name1 = $request->input('secondfixedbill');
        }

        $data = array('name' => $request->input('cname'), 'caddress' => $request->input('caddress'), 'suburb' => $request->input('getsubrub'), 'state' => $request->input('state')
            , 'postcode' => $request->input('pcode'), 'optus' => $request->input('optus'), 'updated_at' => $current);

        DB::table('allprospectuses')->where('id', $id)->update($data);

        // Services Table insert data
        $services = array('level' => $request->input('level'), 'nomobiles' => str_replace('_', '', $request->input('nummobiles')), 'mobcontenddate' => $request->input('contractend'), 'mobilebill' => $name, 'lines' => str_replace('_', '', $request->input('numoflines'))
            , 'fixlineenddate' => $request->input('fixedcontractend'), 'fixedbill' => $name1, 'companyid' => $id, 'updated_at' => $current);
        DB::table('services')->where('companyid', $id)->update($services);

        if (!empty($request->input('notes'))) {
            $followup = str_replace('/', '-', $request->input('followupdate'));
            $time = date(' H:i:s', strtotime($current));
            $followup = date('Y-m-d' . ' ' . $time, strtotime($followup));
            $notes = array('notes' => $request->input('notes'), 'userid' => $request->input('optus'), 'companyid' => $id, 'followupdate' => $followup, 'created_at' => $current, 'updated_at' => $current);
            DB::table('notes')->insert($notes);
        }
    }

    function laod_company_files($id) {
        return DB::table('company_files')
                        ->Join('users', 'company_files.user_id', '=', 'users.id')
                        ->Join('filedescription', 'company_files.file_text', '=', 'filedescription.id')
                        ->select('users.name', 'company_files.company_id', 'company_files.user_id', 'company_files.file_attachment', 'filedescription.description', 'company_files.created_at')
                        ->where('company_files.company_id', '=', $id)
                        ->orderBy('company_files.created_at', 'company_files.user_id')
                        ->get();
    }

    function notesfollowup($id) {
        //echo $id; exit;
        return DB::table('company_files')->select('created_at')->where('company_id', $id)->orderBy('created_at', 'desc')->first();
    }

    public function save_company_images($request) {
        $current = Carbon::now();
        $data = array('description' => $request->input('file_text'));
        $last_insertid = DB::table('filedescription')->insertGetId($data);
        $name_file = '';
        $file = '';
        $files = $request->file('file_attachment');
        $count = 0;
        foreach ($files as $file) {
            if ($count < 5) {
                $destinationPath = 'local/file/company_files';
                $filename = $file->getClientOriginalName();
                //$filename = $filename;
                $upload_success = $file->move($destinationPath, $filename);
//
                $services = array(
                    'company_id' => $request->input('company_id'),
                    'user_id' => $request->input('user_id'),
                    'file_attachment' => $filename,
                    'file_text' => $last_insertid,
                    'created_at' => $current
                );

                DB::table('company_files')->insert($services);
            }
            $count++;
        }
        //exit;
    }

    public function postcontact($request) {
        $current = Carbon::now();
        //print_r($request->input()); exit;
        if ($request->input('default') == '1') {
            $contact_record = DB::table('contact')->select('id')->where('companyid', $request->input('companyid'))->where('default', '1')->get();
            //echo count($contact_record); exit;
            if (count($contact_record) > 0) {
                DB::table('contact')->where('id', $contact_record[0]->id)->update(array('default' => '0'));
                $contact = array('fname' => $request->input('fname'), 'lname' => $request->input('lname'), 'mobile' => $request->input('mobile')
                    , 'email' => $request->input('email'), 'position' => $request->input('position'), 'companyid' => $request->input('companyid'), 'default' => $request->input('default'), 'created_at' => $current, 'updated_at' => $current);
                DB::table('contact')->insert($contact);
            } else {
                $contact = array('fname' => $request->input('fname'), 'lname' => $request->input('lname'), 'mobile' => $request->input('mobile')
                    , 'email' => $request->input('email'), 'position' => $request->input('position'), 'companyid' => $request->input('companyid'), 'default' => $request->input('default'), 'created_at' => $current, 'updated_at' => $current);
                DB::table('contact')->insert($contact);
            }
        } elseif ($request->input('default') == '0') {
            $contact = array('fname' => $request->input('fname'), 'lname' => $request->input('lname'), 'mobile' => $request->input('mobile')
                , 'email' => $request->input('email'), 'position' => $request->input('position'), 'companyid' => $request->input('companyid'), 'default' => $request->input('default'), 'created_at' => $current, 'updated_at' => $current);
            DB::table('contact')->insert($contact);
        }
    }

    public function updatecontact($request, $id) {
        $current = Carbon::now();
        if ($request->input('default') == '1') {
            $contact_record = DB::table('contact')->select('id')->where('companyid', $request->input('companyid'))->where('default', '1')->get();
            if (count($contact_record) > 0) {
                DB::table('contact')->where('id', $contact_record[0]->id)->update(array('default' => '0'));
            } else {
                DB::table('contact')->where('id', $id)->update(array('default' => '1'));
            }
        }
        $contact = array('fname' => $request->input('fname'), 'lname' => $request->input('lname'), 'mobile' => $request->input('mobile')
            , 'email' => $request->input('email'), 'position' => $request->input('position'), 'companyid' => $request->input('companyid'), 'default' => $request->input('default'), 'updated_at' => $current);
        DB::table('contact')->where('id', $id)->update($contact);
    }

    public function default_contact($companyid, $id) {
        $current = Carbon::now();
        // echo $companyid; echo $id;
        $contact_record = DB::table('contact')->select('id')->where('companyid', $companyid)->where('default', '1')->get();
        // echo $contact_record[0]->id;
        //echo count($contact_record); exit;
        if (count($contact_record) > 0) {
            //echo count($contact_record); exit;
            DB::table('contact')->where('id', $contact_record[0]->id)->update(array('default' => '0'));
        } else {
            //echo 'yes'; exit;
            DB::table('contact')->where('id', $id)->update(array('default' => '1'));
        }

        $contact = array('default' => 1, 'updated_at' => $current);
        DB::table('contact')->where('id', $id)->update($contact);
    }

    public function getallprospectus() {
        $companies = DB::table('allprospectuses')
                        ->leftJoin('contact', 'allprospectuses.id', '=', 'contact.companyid')
                        ->leftJoin('users', 'allprospectuses.optus', '=', 'users.id')
                        ->select('users.phone', 'allprospectuses.id', 'allprospectuses.cnotes', 'allprospectuses.name', 'allprospectuses.optus', 'allprospectuses.caddress', 'allprospectuses.suburb', 'contact.mobile', 'contact.fname', 'contact.lname')->groupBy('allprospectuses.id')->orderBy('allprospectuses.id', 'desc')->paginate(50);
        return $companies;
    }

    public function getfollowup() {
        //echo Auth::user()->id; exit;
        $where = array('notes.userid' => Auth::user()->id, 'notes.status' => '0');
        $companies = DB::table('allprospectuses')
                        ->leftJoin('notes', 'allprospectuses.id', '=', 'notes.companyid')
                        ->leftJoin('users', 'allprospectuses.optus', '=', 'users.id')
                        ->select('users.phone', 'allprospectuses.id', 'allprospectuses.cnotes', 'allprospectuses.name', 'allprospectuses.optus', 'allprospectuses.caddress', 'allprospectuses.suburb', 'notes.notes', 'notes.id as noteid', 'notes.followupdate')->where('notes.followupdate', '!=', '')->where($where)->orderBy('notes.followupdate', 'ASC')->paginate(50);
        return $companies;
    }

    public function getallprospectusbyid($id) {
        $companies = DB::table('allprospectuses')
                        ->leftJoin('contact', 'allprospectuses.id', '=', 'contact.companyid')
                        ->leftJoin('notes', 'allprospectuses.id', '=', 'notes.companyid')
                        ->leftJoin('services', 'allprospectuses.id', '=', 'services.companyid')
                        ->leftJoin('users', 'allprospectuses.optus', '=', 'users.id')
                        ->select('users.phone', 'allprospectuses.id', 'allprospectuses.cnotes', 'allprospectuses.name', 'allprospectuses.optus', 'notes.notes', 'allprospectuses.caddress', 'allprospectuses.suburb', 'contact.mobile', 'contact.fname', 'contact.lname', 'services.nomobiles')->where('allprospectuses.optus', $id)->groupBy('allprospectuses.id')->orderBy('allprospectuses.id', 'desc')->paginate(50);
        return $companies;
    }

    public function getnote($id) {
        //echo $id; exit;
        return DB::table('notes')->select('notes', 'followupdate')->where('companyid', $id)->orderBy('created_at', 'desc')->first();
        //print_r($note); exit;
        //return $note;
    }

    public function getnote1($id) {

        return DB::table('notes')->select('notes')->where('companyid', $id)->orderBy('created_at', 'desc')->first();
    }

    public function deletenote($request) {
        $id = $request->input('noteid');
        $data = array('status' => 1);
        DB::table('notes')->where('id', $id)->update($data);
    }

    public function getUsers() {
        $users = DB::table('users')->select('id', 'name')->get();
        return $users;
    }

    public function get_contact($id) {
        //echo $id; exit;
        $contact = DB::table('contact')->select('*')->where('id', $id)->get();
        return $contact;
    }

    public function getsuburb() {
        $users = DB::table('suburb')->select('id', 'suburb')->get();
        return $users;
    }

    public function getsuburbdata($id) {
        $suburb = DB::table('suburb')->select('suburb', 'state', 'postcode')->where('id', $id)->get();
        return $suburb;
    }

    public function getcontacts($id) {
        //echo 'yes'; exit;
        $suburb = DB::table('contact')->select('*')->where('companyid', $id)->orderBy('created_at', 'DESC')->paginate(5);
        return $suburb;
    }

    public function getCompany($id) {
        $company = $users = DB::table('allprospectuses')
                ->leftJoin('services', 'allprospectuses.id', '=', 'services.companyid')
                ->leftJoin('notes', 'allprospectuses.id', '=', 'notes.companyid')
                ->leftJoin('contact', 'allprospectuses.id', '=', 'contact.companyid')
                ->leftJoin('allprospectuses.*', 'contact.*', 'notes.*', 'services.*')->where('allprospectuses.id', $id)
                ->get();
        return $company;
    }

    public function getCompanyy($id) {
        $company = DB::table('allprospectuses')
                ->join('services', 'allprospectuses.id', '=', 'services.companyid')
                ->join('users', 'allprospectuses.optus', '=', 'users.id')
                ->select('allprospectuses.id as prosid', 'allprospectuses.abn', 'allprospectuses.status', 'allprospectuses.name', 'allprospectuses.caddress', 'allprospectuses.suburb', 'allprospectuses.optus', 'allprospectuses.postcode', 'allprospectuses.state', 'allprospectuses.optus', 'services.*', 'users.name as username')->where('allprospectuses.id', $id)->groupBy('allprospectuses.id')
                ->get();
        return $company;
    }

    public function getsummary($request, $start_date, $last_date) {
//        echo $start_date;
//        echo $last_date.'</br>';
//        exit;
        if ($request->input('optus') == 'All') {
            $company = DB::table('allprospectuses')
                            ->join('services', 'allprospectuses.id', '=', 'services.companyid')
                            ->join('users', 'allprospectuses.optus', '=', 'users.id')
                            ->select('allprospectuses.*', 'services.nomobiles', 'services.lines', 'users.name as username')->whereBetween('allprospectuses.created_at', array($start_date, $last_date))->groupBy('allprospectuses.id')
                            ->orderBy('users.name', 'ASC')->get();
        } else {
            $company = DB::table('allprospectuses')
                    ->join('services', 'allprospectuses.id', '=', 'services.companyid')
                    ->join('users', 'allprospectuses.optus', '=', 'users.id')
                    ->select('allprospectuses.*', 'services.nomobiles', 'services.lines', 'users.name as username')->where('allprospectuses.optus', $request->input('optus'))->whereBetween('allprospectuses.created_at', array($start_date, $last_date))->groupBy('allprospectuses.id')
                    ->get();
        }
        //print_r($company); exit;
        return $company;
    }

    public function deleteCompany($id) {
        DB::table('allprospectuses')->where('id', $id)->delete();
    }

    public function deletecontact($id) {
        //echo $id; exit;
        DB::table('contact')->where('id', $id)->delete();
    }

    public function updateUser($request, $id) {
        $data = array('name' => $request->input('name'), 'email' => $request->input('email'), 'address' => $request->input('address'), 'phone' => $request->input('phone')
            , 'role' => $request->input('role'));
        DB::table('users')
                ->where('id', $id)
                ->update($data);
    }

    public function postnote($request, $id) {
        $current = Carbon::now();
        $time = date(' H:i:s', strtotime($current));
        $followup = str_replace('/', '-', $request->input('followupdate'));
        $followup = date('Y-m-d' . ' ' . $time, strtotime($followup));
        $services = array('notes' => $request->input('notes'), 'userid' => $request->input('userid'), 'companyid' => $id, 'followupdate' => $followup, 'created_at' => $current, 'updated_at' => $current);
        DB::table('notes')->insert($services);
        $lastnote = array('cnotes' => $request->input('notes'));
        DB::table('allprospectuses')->where('id', $id)->update($lastnote);
    }

    public function shownote($id) {
        $notes = DB::table('notes')
                        ->join('users', 'notes.userid', '=', 'users.id')
                        ->select('notes.*', 'users.name')->where('notes.companyid', $id)->orderBy('notes.created_at', 'DESC')->paginate(5);
        //$notes = DB::table('notes')->where('companyid', $id)->get();
        return $notes;
    }

}
