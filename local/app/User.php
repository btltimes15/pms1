<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    public function __construct() {
        //$current = Carbon::now();
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function newuser($request) {
        $current = Carbon::now();
        $data = array('name' => $request->input('name'), 'email' => $request->input('email'), 'address' => $request->input('address'), 'phone' => $request->input('phone')
            , 'role' => $request->input('role'), 'password' => bcrypt($request->input('password')), 'created_at' => $current, 'updated_at' => $current);
        DB::table('users')->insert($data);
    }

    public function getUsers() {
        $users = DB::table('users')->select('*')->orderBy('created_at','DESC')->paginate(5);
        //print_r($users); exit;
        return $users;
    }

    public function getUser($id) {
        $users = DB::table('users')->where('id', $id)->get();
        return $users;
    }
    
    public function deleteUser($id) {
        $users = DB::table('users')->where('id', $id)->delete();
        return $users;
    }

    public function updateUser($request, $id) {
        $current = Carbon::now();
        $data = array('name' => $request->input('name'), 'email' => $request->input('email'), 'address' => $request->input('address'), 'phone' => $request->input('phone')
            , 'role' => $request->input('role'), 'updated_at' => $current);
       DB::table('users')
            ->where('id', $id)
            ->update($data);
    }

}
