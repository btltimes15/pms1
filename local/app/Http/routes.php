<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
// Login and Dashboard Route
            Route::get('/', 'Auth\AuthController@getLogin');
            Route::get('auth/login', 'Auth\AuthController@getLogin');
            Route::get('dashboard', 'WelcomeController@index');
            Route::post('login', 'Auth\AuthController@postLogin');
            Route::post('auth/login', 'Auth\AuthController@postLogin');
            Route::get('logout', 'Auth\AuthController@getLogout');
            Route::post('/password/reset', 'Auth/PasswordController@postReset');
            Route::get('/password/reset/{$token}', 'Auth\PasswordController@getReset');
            Route::get('forgotpassord', 'Auth\PasswordController@getEmail');
            Route::post('forgotpassord', 'Auth\PasswordController@postEmail');

            //Add Company Routes
            Route::get('addCompany', 'Allprospectus@index');
            Route::get('reports', 'Allprospectus@reportgenerate');
            Route::post('addCompany', 'Allprospectus@create');
            Route::get('company-edit/{id}', 'Allprospectus@edit');
            Route::get('companydelete/{id}', 'Allprospectus@destroy');
            Route::post('company-update/{id}', 'Allprospectus@update');
            Route::post('getsubrub', 'Allprospectus@getsubrub');
            Route::get('addcontact/{id}', 'Allprospectus@addcontact');
            Route::get('editcontact/{id}/{companyid}', 'Allprospectus@edit_contact');
            Route::post('addcontact', 'Allprospectus@postcontact');
            Route::post('update-contact/{id}', 'Allprospectus@updatecontact');
            Route::get('contact-delete/{id}', 'Allprospectus@deletecontact');


            // Registration routes...
            Route::get('register', 'Auth\AuthController@getRegister');
            Route::post('register', 'RegisterController@create');
            Route::get('adduser', 'UsersController@adduser');
            Route::post('adduser', 'UsersController@create');
            Route::get('users', 'UsersController@show');
           
            Route::get('consultant', 'consultant\Consultant@index');
            Route::get('user-edit/{id}', 'UsersController@edit');
            Route::post('userupdate/{id}', 'UsersController@update');
            Route::get('user-delete/{id}', 'UsersController@destroy');
            
            // Add user note section
            
            Route::get('add-note/{id}', 'UsersController@addnote');
            Route::post('add-note/{id}', 'Allprospectus@postnote');
            Route::get('add-note/{id}', 'UsersController@addnote');
            Route::post('report-detail', 'Allprospectus@detail');
            Route::post('report-summary', 'Allprospectus@summary');
            Route::post('companyfiles', 'Allprospectus@save_company_files');
            Route::post('deletenote', 'Allprospectus@deletenote');
            Route::get('default-contact/{companyid}/{id}', 'Allprospectus@defaultcontact');
            Route::get('email', 'Allprospectus@sendEmailReminder');
            
            // CSV Report
            
            Route::get('csv', 'Allprospectus@export');
            
            Route::get('pdf', function(){
                $pdf = PDF::loadview('admin/pdf');
                return $pdf->download('archivo.pdf');
            });

