<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Allprospectusmodel;
use Auth;
use Carbon\Carbon;
class WelcomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index() {
        $admin_model = new Allprospectusmodel();
        $data['records'] = $admin_model->getallprospectus();
        $data['followup'] = $admin_model->getfollowup();
        $data['userrecord'] = $admin_model->getallprospectusbyid(Auth::user()->id);
        return view('admin/index', $data)->with('notes', new Allprospectusmodel);
        
    }

}
