<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use PDF;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Allprospectusmodel;
use Auth;
use Carbon\Carbon;

class Allprospectus extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->company = new Allprospectusmodel();
    }

    public function index() {
        $users = new User();
        $company = new Allprospectusmodel();
        $data['users'] = $users->getUsers();
        $data['suburb'] = $company->getsuburb();
        return view('admin/addcompany', $data);
    }

    public function getsubrub(Request $request) {
        $company = new Allprospectusmodel();
        $data['suburb'] = $company->getsuburbdata($request->input('id'));
        return response()->json(['state' => $data['suburb'][0]->state, 'postcode' => $data['suburb'][0]->postcode, 'suburb' => $data['suburb'][0]->suburb]);
    }

    public function addcontact($id) {
        $data['id'] = $id;
        return view('admin/contact', $data);
    }

    public function edit_contact($id, $companyid) {
        //echo $companyid;
        $contact = new Allprospectusmodel();
        $data['records'] = $contact->get_contact($id);
        $data['companyid'] = $companyid;
        return view('admin/editcontact', $data);
    }

    public function deletecontact($id) {
        $contact = new Allprospectusmodel();
        $contact->deletecontact($id);
        return redirect('dashboard');
    }

    public function postcontact(Request $request) {
        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'default' => 'required',
        ]);
        $company = new Allprospectusmodel();
        //print_r($request->input()); exit;
        $company->postcontact($request);
        return redirect('addcontact/' . $request->input('companyid'))->with('contactadd', 'One Contact Added Successfully');
    }

    public function updatecontact(Request $request, $id) {
        $company = new Allprospectusmodel();
        $company->updatecontact($request, $id);
        return redirect('editcontact/' . $id . '/' . $request->input('companyid'))->with('updatecontact', 'Contact Updated Successfully');
    }

    public function create(Request $request) {
        //echo $request->input('suburb'); exit;
        $this->validate($request, [
            'cname' => 'required',
            'suburb' => 'required|not_in:Choose Suburb',
            'optus' => 'required|not_in:Choose Consultant',
            'level' => 'required|not_in:Choose Level',
        ]);
        $company = new Allprospectusmodel();
        $last_insert_id = $company->newcompanycreate($request);
        return redirect('dashboard');
    }

    public function print_data($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    public function edit($id) {

        $company = new Allprospectusmodel();
        $data['users'] = $company->getUsers();
        $data['suburb'] = $company->getsuburb();
        $data['records'] = $company->getCompanyy($id);
        $data['contacts'] = $company->getcontacts($id);
        $data['notes'] = $company->shownote($id);
        $data['company_images'] = $company->laod_company_files($id);
        return view('admin/editcompany', $data);
    }

    public function save_company_files(Request $request) {
        $company = new Allprospectusmodel();
        $this->validate($request, [
            'file_attachment' => 'required',
        ]);

        $modal_response = $company->save_company_images($request);
        return redirect('company-edit/' . $request->input('company_id'))->with('file_success', 'One file uploaded Successfully');
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'cname' => 'required',
            'suburb' => 'required|not_in:Choose Suburb',
            'optus' => 'required|not_in:Choose Consultant',
            'level' => 'required|not_in:Choose Level',
        ]);
        $company = new Allprospectusmodel();
        $last_insert_id = $company->updatecompany($request, $id);
        return redirect('company-edit/' . $id)->with('updatecompany', 'Your Company Updated Successfully');
    }

    public function destroy($id) {
        $company = new Allprospectusmodel();
        $company->deleteCompany($id);
        return redirect('dashboard')->with('deletecompany', 'Company deleted Successfully');
    }

    public function shownote($id) {
        $company = new Allprospectusmodel();
        $data['records'] = $company->shownote($id);
        return view('consultant/notes', $data);
    }

    public function postnote(Request $request, $id) {
        $this->validate($request, [
            'notes' => 'required',
        ]);
        $company = new Allprospectusmodel();
        $company->postnote($request, $id);
        return Redirect::back()->with('addnote', 'Note Added Successfully');
    }

    public function reportgenerate() {
        $data['users'] = $this->company->getUsers();
        $data['datestart'] = Carbon::now()->format('d/m/Y');
        $data['dateend'] = Carbon::now()->addMonths(3)->format('d/m/Y');
        //print_r($data['datestart']);
        return view('admin/report', $data);
    }

    public function summary(Request $request) {

        //Explode Date
        $explode_start = explode('/', $request->input('datestart'));
        $explode_end = explode('/', $request->input('dateend'));

        // Parse date and get first date for given month and make mysql date formate
        $dtt = Carbon::parse($explode_start[0] . '-' . $explode_start[1] . '-' . $explode_start[2]);
        $start_date = $dtt->year . '-' . $dtt->month . '-' . '01' . ' ' . '00:00:00';
        // echo $dtt->month;
        // Parse date and get first date for given month and make mysql date formate
        $dt = Carbon::parse($explode_end[0] . '-' . $explode_end[1] . '-' . $explode_end[2]);
        $last_date = $dt->year . '-' . $dt->month . '-' . $dt->daysInMonth . ' ' . '00:00:00';
        // echo $dt->month;
        $ts1 = strtotime($start_date);
        $ts2 = strtotime($last_date);


        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        $month = 0;
        $start_month = '';
        $end_month = '';
        $orignal_date = '';
        $orignal_date1 = '';
        $date_month = '';
        $date_month1 = '';
        for ($i = 0; $i <= $diff; $i++) {
            $orignal_date = Carbon::parse($explode_start[0] . '-' . $explode_start[1] . '-' . $explode_start[2]);
            $orignal_date1 = Carbon::parse($explode_start[0] . '-' . $explode_start[1] . '-' . $explode_start[2]);
            $date_month = $orignal_date->month($orignal_date->month + $i);
            $date = $date_month->year . '-' . $date_month->month . '-' . $date_month->day;
            $date_month1 = $orignal_date1->month($orignal_date->month);
            $start_month = $date_month->startOfMonth();
            $end_month = $date_month1->endOfMonth();

            $data['records'][$date] = $this->company->getsummary($request, $start_month, $end_month);
        }
//        echo '<pre>';
//        var_dump($data['records']); exit;
        $data['optus'] = $request->input('optus');
        $data['date_start'] = $dtt->format(' F Y');
        $data['dateend'] = $dt->format($dt->daysInMonth . ' F Y');

        if ($request->input('type') == 'csv') {
            $csv_filename = $this->export_csv_summary($data);
            return response()->download('local/file/csv/' . $csv_filename);
        } else {
            $this->export_pdf_summary($data);
            //return response()->download('local/file/csv/' . $pdf_filename);
        }
    }

    public function export_pdf_summary() {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->stream();
    }

    public function export_csv_summary($data) {

        // Top Heading
        $list['consultant'] = "Optus Consultant:";
        $list['Optus Consultant'] = $data['optus'];

        // CSV coding
        $filename = time() . 'file.csv';
        $fp = fopen('local/file/csv/' . $filename, 'w');
        fputcsv($fp, $list);
        $date['date'] = "DATE:";
        $date['datevalue'] = '1st ' . $data['date_start'] . ' to ' . $data['dateend'];
        fputcsv($fp, $date);

        $username = '';
        // $total_mobile = '';
        foreach ($data['records'] as $key => $record) {
            // Month Date
            $date = str_replace('-', '/', $key);
            $monthlydate = date('M, Y', strtotime($date));
            $monthdate['month_f'] = '';
            $monthdate['month_date'] = $monthlydate;
            $monthdate['month_l'] = '';
            fputcsv($fp, $monthdate);
            if (count($record) > 0) {
                // Headings
                $header_heading['name'] = "Name";
                $header_heading['overdueheading'] = "Mobile Contracts Overdue";
                $header_heading['nomob'] = "No of Mobiles";
                fputcsv($fp, $header_heading);

                //Values
                $total_mobile = '';

                foreach ($record as $record_month) {

                    if ($record_month->username != $username) {
                        $header_record['username'] = $record_month->username;
                    } else {
                        $header_record['username'] = '';
                    }

                    $header_record['overdue'] = ' ';
                    $header_record['noofmobile'] = $record_month->nomobiles;

                    fputcsv($fp, $header_record);
                    $total_mobile = $total_mobile + $record_month->nomobiles;
                    $username = $record_month->username;
                }
                $total['total_heading'] = 'Total';
                $total['total_overdue'] = ' ';
                $total['total_num'] = $total_mobile;
                fputcsv($fp, $total);
            } else {
                $error['ee'] = 'No Record Found';
                fputcsv($fp, $error);
            }
        }
        $path = $filename;
        return $path;
    }

    public function detail(Request $request) {

        //Explode Date
        $explode_start = explode('/', $request->input('datestart'));
        $explode_end = explode('/', $request->input('dateend'));

        // Parse date and get first date for given month and make mysql date formate
        $dtt = Carbon::parse($explode_start[0] . '-' . $explode_start[1] . '-' . $explode_start[2]);
        $start_date = $dtt->year . '-' . $dtt->month . '-' . '01' . ' ' . '00:00:00';
        // echo $dtt->month;
        // Parse date and get first date for given month and make mysql date formate
        $dt = Carbon::parse($explode_end[0] . '-' . $explode_end[1] . '-' . $explode_end[2]);
        $last_date = $dt->year . '-' . $dt->month . '-' . $dt->daysInMonth . ' ' . '00:00:00';
        // echo $dt->month;
        $ts1 = strtotime($start_date);
        $ts2 = strtotime($last_date);


        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        $month = 0;
        $start_month = '';
        $end_month = '';
        $orignal_date = '';
        $orignal_date1 = '';
        $date_month = '';
        $date_month1 = '';
        for ($i = 0; $i <= $diff; $i++) {
            $orignal_date = Carbon::parse($explode_start[0] . '-' . $explode_start[1] . '-' . $explode_start[2]);
            $orignal_date1 = Carbon::parse($explode_start[0] . '-' . $explode_start[1] . '-' . $explode_start[2]);
            $date_month = $orignal_date->month($orignal_date->month + $i);
            $date = $date_month->year . '-' . $date_month->month . '-' . $date_month->day;
            $date_month1 = $orignal_date1->month($orignal_date->month);
            $start_month = $date_month->startOfMonth();
            $end_month = $date_month1->endOfMonth();

            $data['records'][$date] = $this->company->getsummary($request, $start_month, $end_month);
        }
//        echo '<pre>';
//        var_dump($data['records']);
//         exit;
        $data['optus'] = $request->input('optus');
        $data['date_start'] = $dtt->format(' F Y');
        $data['dateend'] = $dt->format($dt->daysInMonth . ' F Y');
        if ($request->input('type') == 'csv') {
            $csv_filename = $this->export_csv_detail($data);
            return response()->download('local/file/csv/' . $csv_filename);
        } else {
           return $this->export_pdf_detail($data);
        }
    }

    public function export_pdf_detail($data) {
        $pdf = PDF::loadview('admin/pdf',$data);
        return $pdf->download('archivo.pdf');
    }

    public function export_csv_detail($data) {

        // Top Heading
        $list['consultant'] = "Optus Consultant:";
        $list['Optus Consultant'] = $data['optus'];

        // CSV coding
        $filename = time() . 'file.csv';
        $fp = fopen('local/file/csv/' . $filename, 'w');
        fputcsv($fp, $list);
        $date['date'] = "DATE:";
        $date['datevalue'] = '1st ' . $data['date_start'] . ' to ' . $data['dateend'];
        fputcsv($fp, $date);

        $username = '';
        // $total_mobile = '';
        foreach ($data['records'] as $key => $record) {
            // Month Date
            $date = str_replace('-', '/', $key);
            $monthlydate = date('M, Y', strtotime($date));
            $monthdate['month_f'] = '';
            $monthdate['month_date'] = $monthlydate;
            $monthdate['month_l'] = '';
            fputcsv($fp, $monthdate);
            if (count($record) > 0) {
                // Headings
                $header_heading['name'] = "Name";
                $header_heading['overdueheading'] = "Fixed Lines Contracts Overdue";
                $header_heading['nomob'] = "No of Fixed Lines";
                fputcsv($fp, $header_heading);

                //Values
                $total_mobile = '';

                foreach ($record as $record_month) {

                    if ($record_month->username != $username) {
                        $header_record['username'] = $record_month->username;
                    } else {
                        $header_record['username'] = '';
                    }

                    $header_record['overdue'] = ' ';
                    $header_record['noofmobile'] = $record_month->lines;

                    fputcsv($fp, $header_record);
                    $total_mobile = $total_mobile + $record_month->lines;
                    $username = $record_month->username;
                }
                $total['total_heading'] = 'Total';
                $total['total_overdue'] = ' ';
                $total['total_num'] = $total_mobile;
                fputcsv($fp, $total);
            } else {
                $error['ee'] = 'No Record Found';
                fputcsv($fp, $error);
            }
        }
        $path = $filename;
        return $path;
    }

}
