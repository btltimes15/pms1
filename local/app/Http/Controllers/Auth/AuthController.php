<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected $redirectPath = '/dashboard';
    protected $loginPath = '/';
    protected $redirectTo = '/dashboard';

    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'phone' => $data['phone'],
                    'role' => $data['role'],
                    'password' => bcrypt($data['password']),
        ]);
    }
    
    public function createnewuser(Request $request) {
        echo $request->input('email'); exit;
        $this->validate($request, [
        'email' => 'required|email|unique:users',
        'name' => 'required',
        'password' => 'required',
        'role' => 'required',
    ]);
        $userdata = new User();
        $userdata->newuser($request);
        return Redirect::back()->with('msg', 'User Created Successfully');
    }
    

}
