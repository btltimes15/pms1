<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

use App\Allprospectusmodel;
use Auth;
class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {
        if (Auth::user()->role != 'admin')
        {
            return redirect('dashboard');
        }
       return view('admin/signup');
    }
    public function adduser() {
        if (Auth::user()->role == 'user')
        {
            return redirect('dashboard');
        }
        return view('admin/signup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request) {
        if (Auth::user()->role == 'user')
        {
            return redirect('dashboard');
        }
        $this->validate($request, [
        'email' => 'required|email|unique:users',
        'name' => 'required',
        'password' => 'required',
        'role' => 'required',
    ]);
        $userdata = new User();
        $userdata->newuser($request);
        return Redirect::back()->with('msg', 'User Created Successfully');
    }
    
    public function addnote($id){
        $company = new Allprospectusmodel();
        $data['records'] = $company->getCompanyy($id);
        $data['contacts'] = $company->getcontacts($id);
        $data['suburb'] = $company->getsuburb();
        return view('consultant/editcompany', $data);
    }
 
    public function updatecompany(Request $request, $id){
        $this->validate($request, [
        'cname' => 'required',
        'suburb' => 'required|not_in:Choose Suburb',
        'optus' => 'required|not_in:Choose Consultant',
        'level' => 'required|not_in:Choose Level',
    ]);
        $company = new Allprospectusmodel();
        $company->updateUsercompany($request, $id);
        return Redirect::back()->with('addnote', 'Note Added Successfully');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $admin_model = new Company();
        $admin_model->newcompanycreate($request);
        return Redirect::back()->with('msg', 'User Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show() {
        $users = new User();
        $data['records'] = $users->getUsers();
        return view('admin/users', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $users = new User();
        $data['records'] = $users->getUser($id);
        return view('admin/edituser', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
        'email' => 'required',
        'name' => 'required',
        'role' => 'required',
    ]);
        $users = new User();
        $users->updateUser($request, $id);
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //echo $id; exit;
        $users = new User();
        $users->deleteUser($id);
        return redirect('users')->with('deleteuser', 'User has been deleted');
    }

}
