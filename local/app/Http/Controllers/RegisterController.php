<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;


class RegisterController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public function __construct() {
        //$this->middleware('auth');
    }
    
    public function adduser() {
        
        return view('auth/register');
    }
    
    
    public function create(Request $request) {
       //echo $request->input('name'); exit;
        $this->validate($request, [
        'email' => 'required|email|unique:users',
        'name' => 'required',
        'password' => 'required',
        'role' => 'required',
    ]);
        $userdata = new User();
        $userdata->newuser($request);
        return Redirect('/');
    }
    
   

}
